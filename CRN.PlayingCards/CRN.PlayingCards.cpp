// Cael Neary - Playing Cards
// FVTC C++ w/ Ryan Appel Spring 2021

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank {
	JOKER = 0,
	ONE = 1,
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10, 
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14,
	UNDEFINED_RANK = 999
};

enum Suit {
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES,
	JOKERS,
	UNDEFINED_SUIT = 999
};

struct Card {
	Rank rank = UNDEFINED_RANK;
	string RankLabel ="\0";
	Suit suit = UNDEFINED_SUIT;
	string SuitLabel = "\0";
};

int  main()
{


	_getch();
	return 0;

}



